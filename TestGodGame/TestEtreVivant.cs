using Godgame.Libraries;
using NUnit.Framework;
using Godgame.Cons;
using System;
using System.Collections.Generic;

namespace TestGodGame
{
    [TestFixture]
    public class TestEtreVivant
    {
        private Dauphin m_ev;
        private List<EtreVivant> m_listEv;

        [SetUp]
        public void Setup()
        {
            m_ev = new Dauphin("Bobi", new EtreVivant[]{ }, SEXE.Male);
            m_listEv = new List<EtreVivant>()
            {
                new Dauphin("Baptiste",new EtreVivant[]{ }, SEXE.Male),
                new Dauphin("Leon",new EtreVivant[]{ }, SEXE.Male),
                new Dauphin("Dolphi",new EtreVivant[]{ }, SEXE.Femelle),
                new Dauphin("Fougue",new EtreVivant[]{ }, SEXE.Femelle),
                new Corail("Gerard", new EtreVivant[] { }, SEXE.Male),
                new Vegetal("Gerard", new EtreVivant[] { }),
                new Bacterie("Virus",new EtreVivant[]{ }),
                new Bacterie("Virus2",new EtreVivant[]{ }),
            };
        }

        [TearDown]
        public void TearDown()
        {
            m_ev = null;
        }

        [Test]
        public void TestReproduction()
        {
            Exception v_ex;
            Vegetal v_testMort = new Vegetal("pommier", new EtreVivant[] { });
            v_testMort.Die();
            v_ex = Assert.Throws<Exception>(() =>  m_ev.Reproduction(new Dauphin("Gerard", new EtreVivant[] { }, SEXE.Male)));
            Assert.AreEqual(v_ex.Message, " ne peut se reproduire avec un individu de m�me sexe !");

            Assert.DoesNotThrow(() => m_ev.Reproduction(new Dauphin("Camille", new EtreVivant[] { }, SEXE.Femelle)));

            v_ex = Assert.Throws<Exception>(() => m_ev.Reproduction(new Corail("Gerard", new EtreVivant[] { }, SEXE.Male)));
            Assert.AreEqual(v_ex.Message, " ne peut pas se reproduire avec une autre esp�ce !");

            v_ex = Assert.Throws<Exception>(() => v_testMort.Reproduction(m_listEv[5]));
            Assert.AreEqual(v_ex.Message, " ne peut pas se reproduire car l'un des deux est mort !");

            v_ex = Assert.Throws<Exception>(() => m_listEv[6].Reproduction(m_listEv[7]));
            Assert.AreEqual(v_ex.Message, " ne peut pas se reproduire !");
        }

        [Test]
        public void TestSearchSomeone()
        {
            Exception v_ex;

            List<EtreVivant> m_list = new List<EtreVivant>()
            {
                new Corail("Gerard", new EtreVivant[] { }, SEXE.Male),
                new Corail("Galopin", new EtreVivant[] { }, SEXE.Male),
            };

            v_ex = Assert.Throws<Exception>(() => m_listEv[4].SearchSomeone(m_listEv));
            Assert.AreEqual(v_ex.Message, " ne peut pas se reproduire avec une autre esp�ce !");

            v_ex = Assert.Throws<Exception>(() => m_listEv[4].SearchSomeone(m_list));
            Assert.AreEqual(v_ex.Message, " n'a trouv� personne pour se reproduire !");

            m_list.Add(new Corail("Justine",new EtreVivant[] { },SEXE.Femelle));
            Assert.DoesNotThrow(() => m_listEv[4].SearchSomeone(m_list));
        }

        [Test]
        public void TestDivision()
        {
            Exception v_ex;
            v_ex = Assert.Throws<Exception>(() => m_ev.Division(m_ev));
            Assert.AreEqual(v_ex.Message, " ne peut pas se diviser !");

            Assert.DoesNotThrow(() => m_listEv[6].Division(m_listEv[6]));
        }
    }
}