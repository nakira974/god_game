﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Godgame.Libraries
{
    public class Corail : Animal
    {
        public Corail(string p_name, EtreVivant[] p_parents, SEXE p_sexe) : base(p_name, p_parents, p_sexe)
        {
        }
        /// <summary>
        /// Le corail ne peut se déplacer et lève une exception
        /// </summary>
        /// <returns></returns>
        public sealed override string Move()
        {
            throw new Exception($"{this} ne peut pas se déplacer !");
        }

        /// <summary>
        /// Affichage du corail
        /// </summary>
        /// <returns>le message à afficher</returns>
        public override string ToString()
        {
            return $"Un corail nommé {Name}";
        }
    }
}
