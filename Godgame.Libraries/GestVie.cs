﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Godgame.Libraries
{
    public class GestVie
    {
        /// <summary>
        /// Liste des êtres vivants du monde
        /// </summary>
        private List<EtreVivant> m_population;

        /// <summary>
        /// Nom du monde
        /// </summary>
        private string m_wolrdName;

        /// <summary>
        /// Initialise le monde avec un nombre déterminé d'êtres vivants par espèce 
        /// </summary>

        public GestVie(string p_wolrdName, int nb_StartPeople)
        {
            m_population = new List<EtreVivant>();
            m_wolrdName = p_wolrdName;
            _InitPeople(nb_StartPeople);
        }

        /// <summary>
        /// Ajouter un EtreVivant au monde
        /// </summary>
        /// <param name="p_livingThing">l'être vivant à ajouter</param>
        public void AddToWorld(EtreVivant p_livingThing)
        {
            if (p_livingThing == null) throw new ArgumentException();
            m_population.Add(p_livingThing);
        }

        /// <summary>
        /// Effectue la reproduction d'un pourcentage aléatoire de la population et renvoie les nouveaux êtres vivants
        /// </summary>
        /// <returns>Êtres vivants qui viennent de naître</returns>
        public List<EtreVivant> Reproduction()
        {
            Random v_rng = new Random();
            int v_percentToReproduct;
            int v_nbReproducted = 0;
            int v_index;
            int v_typeReproduction;
            EtreVivant v_currentEV;
            List<EtreVivant> v_newEVs = new List<EtreVivant>();
            EtreVivant v_newEV;

            v_percentToReproduct = v_rng.Next(20, 40);

            do
            {
                v_typeReproduction = v_rng.Next(0, 2);
                v_index = v_rng.Next(0, m_population.Count);
                v_currentEV = m_population[v_index];
                if (v_currentEV.IsAlive())
                {
                    if (v_typeReproduction == 1)
                    {
                        v_newEV = DoReproduction(v_currentEV);
                        if (v_newEV == null)
                        {
                            v_newEV = DoDivision(v_currentEV);
                            if (v_newEV != null)
                            {
                                v_newEVs.Add(v_newEV);
                                v_nbReproducted++;
                            }
                        }
                        else
                        {
                            v_newEVs.Add(v_newEV);
                            v_nbReproducted++;
                        }
                    }
                    else
                    {
                        v_newEV = DoDivision(v_currentEV);
                        if (v_newEV == null)
                        {
                            v_newEV = DoReproduction(v_currentEV);
                            if (v_newEV != null)
                            {
                                v_newEVs.Add(v_newEV);
                                v_nbReproducted++;
                            }
                        }
                        else
                        {
                            v_newEVs.Add(v_newEV);
                            v_nbReproducted++;
                        }
                    }
                }
            } while (v_nbReproducted < NbVivant() * ((double)v_percentToReproduct / 100));

            m_population.AddRange(v_newEVs);
            return v_newEVs;
        }

        /// <summary>
        /// Effectue un massacre d'un certain pourcentage de la population et retourne ceux qui ont été tués
        /// </summary>
        /// <returns>Liste des êtres vivants qui ont été tués</returns>
        public List<EtreVivant> Kill()
        {
            Random v_rng = new Random();
            List<EtreVivant> v_listDead = new List<EtreVivant>();
            int v_percentToDie;
            int v_index;
            int v_nbdied = 0;

            EtreVivant v_currentEV;
            v_percentToDie = v_rng.Next(10, 33);
            do
            {
                v_index = v_rng.Next(0, m_population.Count);
                v_currentEV = m_population[v_index];
                if (v_currentEV.IsAlive())
                {
                    v_currentEV.Die();
                    v_listDead.Add(v_currentEV);
                    v_nbdied++;
                }
            } while (v_nbdied < NbVivant() * ((double)v_percentToDie / 100));
            return v_listDead;
        }

        /// <summary>
        /// Obtenir la liste des êtres vivants de type T
        /// </summary>
        /// <typeparam name="T">La classe des êtres vivants à obtenir</typeparam>
        /// <returns>La liste des êtres vivants recherchés</returns>
        public List<T> GetListOf<T>(T p_detectClass) where T : EtreVivant
        {
            List<T> v_listOfType = new List<T>();

            for (int i = 0; i < m_population.Count; i++)
            {
                if (m_population[i].GetType() == p_detectClass.GetType() && m_population[i].IsAlive()
                ) //Est du type recherché ET est en vie
                {
                    v_listOfType.Add((T)m_population[i]);
                }
            }

            if (v_listOfType.Count == 0) throw new Exception("n'a trouvé personne pour se reproduire !");

            return v_listOfType;
        }

        /// <summary>
        /// Obtenir la liste des êtres vivants mort ou vif
        /// </summary>
        /// <typeparam name="EtreVivant">Les indivuds ayant vécu ou vivant</typeparam>
        /// <returns>La liste des êtres vivants du monde </returns>
        public List<EtreVivant> Get_EveryBody()
        {
            return m_population;
        }


        /// <summary>
        /// Retourne le nombre d'être encore en vie dans le monde
        /// </summary>
        /// <returns></returns>
        public int NbVivant()
        {
            int v_nbVivant = 0;

            for (int i = 0; i < m_population.Count; i++)
            {
                if (m_population[i].IsAlive()) v_nbVivant++;
            }

            return v_nbVivant;
        }

        /// <summary>
        /// Initialise le monde avec X êtres vivants de chaque espèce
        /// </summary>
        /// <param name="x">nombre d'êtres vivants par espèce</param>
        private void _InitPeople(int x)
        {

            int v_nbPeople = x;

            m_population.Add(new Geochelone("Georges", new EtreVivant[] { }, SEXE.Male));


            for (int i = 0; i < v_nbPeople; i++)
            {
                Bacterie v_bacterie = new Bacterie(EtreVivant.Set_Name(), new EtreVivant[] { });
                m_population.Add(v_bacterie);
                Corail v_corail = new Corail(EtreVivant.Set_Name(), new EtreVivant[] { }, EtreVivant.RandomSexe());
                m_population.Add(v_corail);
                Dauphin v_dauphin = new Dauphin(EtreVivant.Set_Name(), new EtreVivant[] { }, EtreVivant.RandomSexe());
                m_population.Add(v_dauphin);
                Humain v_humain = new Humain(EtreVivant.Set_Name(), new EtreVivant[] { }, EtreVivant.RandomSexe());
                m_population.Add(v_humain);
                Champignon v_champignon = new Champignon(EtreVivant.Set_Name(), new EtreVivant[] { }, EtreVivant.RandomSexe());
                m_population.Add(v_champignon);
            }


        }

        /// <summary>
        /// Effectue la reproduction d'un individu de la recherche à l'accouplement
        /// </summary>
        /// <param name="p_ev">L'individu qui va se reproduire</param>
        /// <returns>Le nouveau né</returns>
        private EtreVivant DoReproduction(EtreVivant p_ev)
        {
            EtreVivant v_newEv = null;
            EtreVivant v_reproductWith;
            try
            {
                v_reproductWith = p_ev.SearchSomeone(GetListOf(p_ev));
                v_newEv = p_ev.Reproduction(v_reproductWith);
            }
            catch (Exception e)
            {
                Console.WriteLine($"{p_ev}{e.Message}");
            }
            return v_newEv;
        }

        /// <summary>
        /// Effectue la division d'un individu
        /// </summary>
        /// <param name="p_ev">L'individu qui va se diviser</param>
        /// <returns>Le nouvel individu</returns>
        private EtreVivant DoDivision(EtreVivant p_ev)
        {
            EtreVivant v_newEv = null;
            try
            {
                v_newEv = p_ev.Division(p_ev);
            }
            catch (Exception e)
            {
                Console.WriteLine($"{p_ev}{e.Message}");
            }
            return v_newEv;
        }
    }
}


