﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Godgame.Libraries
{
    public abstract class Animal : EtreVivant, IReproduisible
    {
        public Animal(string p_name, EtreVivant[] p_parents, SEXE p_sexe) : base(p_name, p_parents, p_sexe)
        {
        }

        /// <summary>
        /// Un animal doit pouvoir se délacer
        /// </summary>
        /// <returns>le message à afficher pour signaler le déplacement</returns>
        public abstract string Move();
    }
}
