﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Godgame.Libraries
{
    public class Dauphin : Animal
    {

        public Dauphin(string p_name, EtreVivant[] p_parents, SEXE p_sexe) : base(p_name, p_parents, p_sexe)
        {
        }

        /// <summary>
        /// Comment se déplace un dauphin
        /// </summary>
        /// <returns>le message à afficher pour signaler le déplacement</returns>
        public override string Move()
        {
            return $"{Name} le dauphin nage !";
        }

        /// <summary>
        /// Affichage d'un dauphin
        /// </summary>
        /// <returns>le message à afficher</returns>
        public override string ToString()
        {
            return $"Un dauphin nommé {Name}";
        }
    }
}
