﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Godgame.Libraries
{
    public class Vegetal : EtreVivant, IDivisible, IReproduisible
    {
        public Vegetal(string p_name, EtreVivant[] p_parents) : base(p_name, p_parents, SEXE.Hermaphrodite)
        {
        }

        /// <summary>
        /// Affichage d'un végétal
        /// </summary>
        /// <returns>le message à afficher</returns>
        public override string ToString()
        {
            return $"Un végétal nommé {Name}";
        }
    }
}
