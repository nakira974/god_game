﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Godgame.Libraries
{
    public class Geochelone : Animal
    {
        public Geochelone(string p_name, EtreVivant[] p_parents, SEXE p_sexe) : base(p_name, p_parents, p_sexe)
        {
        }

        /// <summary>
        /// Comment une tortue Geochelone se déplace
        /// </summary>
        /// <returns>le message à afficher pour signaler le déplacement</returns>
        public sealed override string Move()
        {
            return $"{this} se déplace en marchant lentement.";
        }

        /// <summary>
        /// Affichage d'une Geochelone
        /// </summary>
        /// <returns>le message à afficher</returns>
        public override string ToString()
        {
            return $"Un Geochelone nommé {Name}";
        }
    }
}
