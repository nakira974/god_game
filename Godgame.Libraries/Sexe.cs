﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Godgame.Libraries
{
    /// <summary>
    /// Enumération des différents sexes possibles
    /// </summary>
    public enum SEXE
    {
        Male,
        Femelle,
        Hermaphrodite
    }
}
