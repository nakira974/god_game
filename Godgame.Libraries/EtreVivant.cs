﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Godgame.Libraries
{
    public abstract class EtreVivant
    {
        /// <summary>
        /// Le nom de l'être vivant
        /// </summary>
        private readonly string m_name;

        /// <summary>
        /// Est-il en vie
        /// </summary>
        public bool m_alive;

        /// <summary>
        /// Le ou les parents de l'être vivant
        /// </summary>
        private EtreVivant[] m_parents;

        /// <summary>
        /// Le sexe de l'être vivant
        /// </summary>
        private SEXE m_sexe;

        public EtreVivant(string p_name, EtreVivant[] p_parents, SEXE p_sexe)
        {
            this.m_name = p_name;
            this.m_alive = true;
            this.m_parents = p_parents;
            this.m_sexe = p_sexe;
        }

        public string Name
        {
            get { return m_name; }
        }

        public SEXE Sexe
        {
            get { return m_sexe; }
        }

        public bool IsAlive()
        {
            return m_alive;
        }

        public abstract override string ToString();

        /// <summary>
        /// Tue l'être vivznt
        /// </summary>
        public void Die()
        {
            m_alive = false;
        }

        /// <summary>
        /// Reproduit un être vivant si possible sinon renvoie des exceptions
        /// </summary>
        /// <typeparam name="T">le type de l'être vivant</typeparam>
        /// <param name="p_individual2">l'être vivant avec qui this vas se reproduire</param>
        /// <returns>un nouvel être vivant</returns>
        public T Reproduction<T>(T p_individual2) where T : EtreVivant
        {
            EtreVivant v_new;

            if (!(this is IReproduisible))
                throw new Exception(" ne peut pas se reproduire !");

            if (!p_individual2.IsAlive() || !this.IsAlive())
                throw new Exception(" ne peut pas se reproduire car l'un des deux est mort !");

            if (p_individual2.GetType().Name != this.GetType().Name) //si pas le même type 
            {
                throw new Exception(" ne peut pas se reproduire avec une autre espèce !");
            }

            if (this.m_sexe == p_individual2.m_sexe && this.m_sexe != SEXE.Hermaphrodite && p_individual2.m_sexe != SEXE.Hermaphrodite) // si du même sexe
            {
                throw new Exception(" ne peut se reproduire avec un individu de même sexe !");
            }

            v_new = (EtreVivant)Activator.CreateInstance(p_individual2.GetType(), Set_Name(), new EtreVivant[] { this, p_individual2 }, RandomSexe());

            return (T)v_new;
        }

        /// <summary>
        /// Cherche un individu du même type, de sexe compatible et en vie pour se reproduire
        /// </summary>
        /// <typeparam name="T">Espèce des individus</typeparam>
        /// <param name="p_individuals">la liste d'individus parmi lesquels il faut choisir un/une partenaire</param>
        /// <returns>un individu compatible pour se reproduire</returns>
        public T SearchSomeone<T>(List<T> p_individuals) where T : EtreVivant
        {
            if (!ListOk(p_individuals)) throw new Exception(" ne peut pas se reproduire avec une autre espèce !");
            T v_toReproductWith;
            List<T> v_canReproductWith = new List<T>();
            Random v_rng = new Random();
            int v_index;

            for (int i = 0; i < p_individuals.Count; i++)
            {
                if (p_individuals[i].Sexe != this.Sexe || p_individuals[i].Sexe == SEXE.Hermaphrodite || this.Sexe == SEXE.Hermaphrodite)
                    v_canReproductWith.Add(p_individuals[i]);
            }

            if (v_canReproductWith.Count == 0)
            {
                throw new Exception(" n'a trouvé personne pour se reproduire !");
            }

            v_index = v_rng.Next(0, v_canReproductWith.Count);
            v_toReproductWith = v_canReproductWith[v_index];

            return v_toReproductWith;
        }

        /// <summary>
        /// Choisit un sexe random
        /// </summary>
        /// <returns>retourne le sexe choisi</returns>
        public static SEXE RandomSexe()
        {
            Random v_rng = new Random();
            return (SEXE)v_rng.Next(0, 2);
        }

        /// <summary>
        /// Fait la division d'un être vivant 
        /// </summary>
        /// <typeparam name="T">type de l'être vivant </typeparam>
        /// <param name="p_detectClassUsed">même être que this, permet de détecter le type à retourner</param>
        /// <returns>Le nouvel être vivant</returns>
        public T Division<T>(T p_detectClassUsed) where T : EtreVivant
        {
            EtreVivant v_new;

            if (!(this is IDivisible)) throw new Exception(" ne peut pas se diviser !");
            v_new = (EtreVivant)Activator.CreateInstance(p_detectClassUsed.GetType(), Set_Name(), new EtreVivant[] { this });

            return (T)v_new;
        }

        /// <summary>
        /// Choisir un nom random dans deux csv (noms et prénoms)
        /// </summary>
        /// <returns>le nom généré</returns>
        public static string Set_Name()
        {
            string v_name = null;
            Random v_random = new Random();
            List<string> v_listLastNames; //nom
            List<string> v_listFirstNames; //prenom

            v_listLastNames = ReadCsv((Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "/../../Godgame.Libraries" + "/Ressources/noms.csv"));
            v_listFirstNames = ReadCsv((Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "/../../Godgame.Libraries" + "/Ressources/Prenoms.csv"));

            v_name = $"{v_listFirstNames[v_random.Next(0, v_listFirstNames.Count)]} {v_listLastNames[v_random.Next(0, v_listLastNames.Count)]}";


            return v_name;
        }

        /// <summary>
        /// Permet de lire un csv
        /// </summary>
        /// <param name="path">le chemin vers le csv</param>
        /// <returns>la liste contenu dans le csv</returns>
        public static List<string> ReadCsv(string path)
        {
            List<string> v_list = new List<string>();
            StreamReader v_reader = new StreamReader(File.OpenRead(path));

            while (!v_reader.EndOfStream)
            {
                v_list.Add(v_reader.ReadLine());
            }
            v_reader.Close();
            return v_list;
        }

        /// <summary>
        /// Vérifie qu'une liste contient bien des individus de la même espèce que this
        /// </summary>
        /// <typeparam name="T">type de la liste</typeparam>
        /// <param name="p_individuals"> la liste</param>
        /// <returns>True si la liste est bonne false sinon</returns>
        private bool ListOk<T>(List<T> p_individuals) where T : EtreVivant
        {
            bool v_res = true;

            foreach(T ev in p_individuals)
            {
                if (ev.GetType().Name != this.GetType().Name) v_res = false;
            }
            return v_res;
        }

    }
}
