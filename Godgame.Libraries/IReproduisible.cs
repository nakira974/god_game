﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Godgame.Libraries
{
    /// <summary>
    /// Si une classe implémente cette interface, cela veut dire que celle-ci peut se reproduire
    /// </summary>
    public interface IReproduisible
    {
    }
}
