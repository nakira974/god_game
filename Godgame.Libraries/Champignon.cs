namespace Godgame.Libraries
{
    public class Champignon : EtreVivant
    {
        public Champignon(string p_name, EtreVivant[] p_parents, SEXE p_sexe) : base(p_name, p_parents, p_sexe)
        {
        }

        /// <summary>
        /// Le champignon affiche qui il est
        /// </summary>
        /// <returns>Le message à afficher</returns>
        public override string ToString()
        {
            return $"Un champignon nommé {Name}";
        }
    }
}
