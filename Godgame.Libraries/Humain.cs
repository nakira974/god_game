using System;
using System.Collections.Generic;
using System.Text;

namespace Godgame.Libraries
{
    public class Humain : Animal
    {
        public Humain(string p_name, EtreVivant[] p_parents, SEXE p_sexe) : base(p_name, p_parents, p_sexe)
        {
        }

        /// <summary>
        /// Comment un humain se déplace t-il
        /// </summary>
        /// <returns>le message à afficher pour signaler le déplacement</returns>
        public override string Move()
        {
            return $"{Name} se déplace en marchant !";
        }

        /// <summary>
        /// Affichage de l'humain en fonction de son sexe
        /// </summary>
        /// <returns>le message à afficher</returns>
        public override string ToString()
        {
            string res;
            if (Sexe == SEXE.Male)
            {
                res = $"Un humain nommé {Name}";
            }
            else if (Sexe == SEXE.Femelle)
            {
                res = $"Une humaine nommée {Name}";
            }
            else
            {
                res = $"Un(e) humain(e) nommé(e) {Name}";
            }
            return res;
        }

    }
}