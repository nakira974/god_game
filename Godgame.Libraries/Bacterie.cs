﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Godgame.Libraries
{
    public class Bacterie : EtreVivant, IDivisible
    {
        public Bacterie(string p_name, EtreVivant[] p_parents) : base(p_name, p_parents, SEXE.Hermaphrodite)
        {
        }

        /// <summary>
        /// Comment se déplace une bactérie
        /// </summary>
        /// <returns>le message à afficher pour signaler le déplacement</returns>
        public string Move()
        {
            return $"{Name} La bactérie flagèle !";
        }

        /// <summary>
        /// Affichage d'une bactérie
        /// </summary>
        /// <returns>le message à afficher</returns>
        public override string ToString()
        {
            return $"Une bactérie nommée {Name}";
        }
    }
}
