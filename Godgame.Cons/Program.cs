﻿using System;
using System.Collections.Generic;
using Godgame.Libraries;

namespace Godgame.Cons
{
    class Program
    {
        static void Main(string[] args)
        {
            GestVie v_world = new GestVie("World", 4);
            List<EtreVivant> v_listEVToDisplay;
            List<EtreVivant> dies = new List<EtreVivant>();

            bool v_wantToLeave = false;
            bool v_actionInputOk = false;

            char v_action;


            Console.Write("Bienvenue dans le simulateur de gestion de vie de créatures : (Appuyez sur une touche)...");
            Console.ReadKey();

            do
            {
                Console.Clear();
                Console.WriteLine($"Il y a {v_world.NbVivant()} individus en vie sur les {v_world.Get_EveryBody().Count} qui ont parcourus ce monde.");

                do
                {
                    Console.WriteLine("Voici les actions pouvant être réalisées :");
                    Console.WriteLine("T : Tuer");
                    Console.WriteLine("V : Voir le monde");
                    Console.WriteLine("R : Reproduire");
                    Console.WriteLine("D : Afficher les morts");
                    Console.WriteLine("Q : Quitter");
                    Console.WriteLine("Veuillez saisir l'action à effectuer :");

                    v_actionInputOk = char.TryParse(Console.ReadLine(), out v_action);

                } while (v_action != 'T' && v_action != 't' && v_action != 'R' && v_action != 'r' && v_action != 'Q' && v_action != 'V' && v_action != 'v' && v_action != 'D' && v_action != 'd' && v_action != 'q' && !v_actionInputOk);

                if (v_action == 'T' || v_action == 't')
                {
                    v_listEVToDisplay = v_world.Kill();
                    Console.WriteLine($"\nIl y a eu {v_listEVToDisplay.Count} morts :");
                    foreach (EtreVivant ev in v_listEVToDisplay)
                    {
                        if (ev.Sexe == SEXE.Male)
                            Console.WriteLine($"{ev} est mort.");
                        else if (ev.Sexe == SEXE.Femelle)
                            Console.WriteLine($"{ev} est morte.");
                        else
                            Console.WriteLine($"{ev} est mort(e).");
                    }
                    dies.AddRange(v_listEVToDisplay);
                }
                else if (v_action == 'R' || v_action == 'r')
                {
                    v_listEVToDisplay = v_world.Reproduction();
                    Console.WriteLine($"\nIl y a eu {v_listEVToDisplay.Count} naissances :");
                    foreach (EtreVivant ev in v_listEVToDisplay)
                    {
                        if (ev.Sexe == SEXE.Male)
                            Console.WriteLine($"{ev} est né.");
                        else if (ev.Sexe == SEXE.Femelle)
                            Console.WriteLine($"{ev} est née.");
                        else
                            Console.WriteLine($"{ev} est né(e).");
                    }
                }
                else if (v_action == 'Q' || v_action == 'q')
                {
                    v_wantToLeave = true;
                }
                else if (v_action == 'V' || v_action == 'v')
                {
                    foreach (EtreVivant someone in v_world.Get_EveryBody())
                    {
                        Console.WriteLine(someone);

                    }
                    Console.WriteLine("\nIl y a eu : " + v_world.Get_EveryBody().Count + " êtres vivants");
                }
                else if (v_action == 'D' || v_action == 'd')
                {
                    foreach (var die in dies)
                    {
                        Console.WriteLine(die + " est mort...");
                        Console.WriteLine();
                    }
                    Console.WriteLine("Il y a eu : " + dies.Count + "  mort(s) dans ce monde");
                }
                Console.ReadKey();
                if (v_world.NbVivant() <= 0)
                {
                    Console.WriteLine("Tout le monde est mort ...\n" +
                        "La partie est finie ...");
                }
            } while (!v_wantToLeave  && v_world.NbVivant() > 0);
        }
    }
}
